﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BookStore.Tests
{
    [TestClass]
    public class BookStoreTests
    {
        [TestMethod]
        public void ChangeBookCountTest()
        {
            //arrange
            Book book = new Book() { _name = "test", _author = "test", _description = "test", _count = 5, _price = 10 };
            int c = 3;

            //act
            book.ChangeBookCount(c);
            //assert
            Assert.AreEqual(c, book._count);
        }
        [TestMethod]
        public void ChangeBookPrice()
        {
            //arrange
            Book book = new Book() { _name = "test", _author = "test", _description = "test", _count = 5, _price = 10 };
            int p = 20;

            //act
            book.ChangePrice(p);

            //assert
            Assert.AreEqual(p, book._price);
        }
        [TestMethod]
        public void CreateBookTest()
        {
            //arrange
            Book book = new Book() { _name = "test", _author = "test", _description = "test", _count = 5, _price = 10 };
            BookList blist = new BookList();
            //act
            blist.AddBookToList(book);
            //assert
            Assert.IsNotNull(blist);
        }
    }
}
