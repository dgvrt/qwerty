﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BookStore
{
    /// <summary>
    /// Class conducts object serialisation and deserialization
    /// </summary>
    class Serialization
    {
        public static Task SerializedAsync(BookList bookList)
        {
            XmlSerializer xs = new XmlSerializer(typeof(BookList));
            return Task.Run(() =>
            {
                using (FileStream fs = new FileStream($"XMLBookListObj.XML", FileMode.OpenOrCreate, FileAccess.Write))
                {
                    xs.Serialize(fs, bookList);
                }
            });
        }
        public static Task SerializedAsync(List<User> ulist)
        {
            XmlSerializer xs = new XmlSerializer(typeof(List<User>));
            return Task.Run(() =>
            {             
                using (FileStream fs = new FileStream($"XMLUserListObj.XML", FileMode.OpenOrCreate, FileAccess.Write))
            {
                
                xs.Serialize(fs, ulist);
            }
            });
        }

        public static Task<BookList> BookListDeserializedAsync()
        {
            return Task.Run(() =>
            {
                using (FileStream fs = new FileStream($"{ Directory.GetCurrentDirectory() }\\XMLBookListObj.XML", FileMode.OpenOrCreate))
                {
                    XmlSerializer xs = new XmlSerializer(typeof(BookList));
                    BookList obj;
                    obj = (BookList)xs.Deserialize(fs);
                    return obj;
                }
            });
        }
        public static Task<List<User>> UserListDeserializedAsync()
        {
            return Task.Run(() =>
            {
                using (FileStream fs = new FileStream($"{Directory.GetCurrentDirectory() }\\XMLUserListObj.XML", FileMode.OpenOrCreate))
                {
                    XmlSerializer xs = new XmlSerializer(typeof(List<User>));
                    List<User> obj;
                    obj = (List<User>)xs.Deserialize(fs);
                    return obj;
                }
            });
        }
    }
}
