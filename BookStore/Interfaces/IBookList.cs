﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Interfaces
{
    interface IBookList
    {
        void AddBookToList(Book book);
        void DeleteBookFromList(Book book);
        Book SearchBook(string name);
    }
}
