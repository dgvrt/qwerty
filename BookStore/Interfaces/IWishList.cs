﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Interfaces
{
    interface IWishList
    {
        void AddBookToWishList(Book b);
        void DeleteBookFromWishList(Book b);
        List<Book> GetWishList();
        void PrintWishList();
    }
}
