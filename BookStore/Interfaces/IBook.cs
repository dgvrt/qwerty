﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Interfaces
{
    interface IBook
    {
        void ChangePrice(double p);
        void ChangeDescription(string d);
        void ChangeBookCount(int i);
    }
}
