﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Interfaces
{
    interface IComment
    {
        void AddComment(Comment comment);
        void DeleteComment(Comment comment);
        void PrintComments();
    }
}
