﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataIO
{
    class DataWriter
    {
        public static async void DataWriteAsync(BookList blist, List<User> ulist)
        {
            Task BookWriter = Serialization.SerializedAsync(blist);
            Task UserWriter = Serialization.SerializedAsync(ulist);
            await Task.WhenAll(new [] { BookWriter, UserWriter });
        }
    }
}
