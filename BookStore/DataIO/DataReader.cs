﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BookStore.DataIO
{
    static class DataReader
    {
        public static async Task<BookList> BooksDataReadAsync() 
        {

            if (File.Exists($"{Directory.GetCurrentDirectory()}\\XMLBookListObj.XML"))
            {
                return await Serialization.BookListDeserializedAsync();
                //return result;
            }
            else
            {
                return await Task.Run(() =>
                {
                    BookList blist = new BookList();
                    blist.AddBookToList(new Book() { _name = "test1", _author = "test1", _description = "test1", _count = 3, _price = 50 });
                    blist.AddBookToList(new Book() { _name = "test2", _author = "test2", _description = "test2", _count = 2, _price = 76 });
                    blist.AddBookToList(new Book() { _name = "test3", _author = "test3", _description = "test3", _count = 10, _price = 110 });
                    return blist;
                });
            }
        }
        public static async Task<List<User>> UsersDataReadAsync()
        {
            if (File.Exists($"{Directory.GetCurrentDirectory()}\\XMLUserListObj.XML"))
            {
                return await Serialization.UserListDeserializedAsync();
                //return result;
            }
            else
            {
               return await Task.Run(() =>
                {
                    return new List<User>() { new User() {_id = 0, _login = "admin", _password = "password", _nickname = "admin",
                        _wallet = 1000, _access = 1} };
                });
            }
        }
    }
}
