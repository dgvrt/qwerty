﻿using BookStore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// Class is a list of all available books in the store.
/// </summary>
namespace BookStore
{
    [Serializable]
    public class BookList : IBookList, IPrint
    {
        /// <summary>
        /// List of all books in the store
        /// </summary>
        public List<Book> _bList;
        public event EventHandler<LogerEventArgs> LogOn;
        /// <summary>
        /// Initializes a new object
        /// </summary>
        public BookList()
        {
            _bList = new List<Book>();
        }
        /// <summary>
        /// Method search ф book in the book list
        /// </summary>
        /// <param name="name">The name of the book you are looking for</param>
        /// <returns>The book you searched for</returns>
        public Book SearchBook(string name)
        {
            if (_bList.Find(x => x._name == name) != null)
            {
                return _bList.Find(x => x._name == name);
            }
            else
            {
                Console.WriteLine(Properties.Resource.BookNotFind);
                NLoger._logger.Error(Properties.Resource.BookNotFind);
            }
            return null;
        }
        /// <summary>
        /// The method checks if such a book already exists in the list, if not, then adds it
        /// </summary>
        /// <param name="book">Book to add to the list</param>
        public void AddBookToList(Book book)
        {
            try
            {
                if (!_bList.Any(x => x._name == book._name))
                {
                    _bList.Add(book);
                    Console.WriteLine(Properties.Resource.BookhasBemmSuccessfullyAdded);
                    LogOn?.Invoke(this, new LogerEventArgs() { _comment = $"{Properties.Resource.Book} {book._name} {Properties.Resource.addToStore}" });
                }
                else Console.WriteLine(Properties.Resource.BookAlreadyExist);
            }
            catch (Exception ex)
            {
                NLoger._logger.Error(ex.Message);
            }
        }
        public void AddBookToList()
        {
            try
            {
                Book book = new Book();
                {
                    double price;
                    int count;
                    Console.WriteLine(Properties.Resource.EnterNameOfBook);
                    book._name = Console.ReadLine();
                    Console.WriteLine(Properties.Resource.EnterAuthorOfBook);
                    book._author = Console.ReadLine();
                    Console.WriteLine(Properties.Resource.EnterDescriptionOFBook);
                    book._description = Console.ReadLine();
                    Console.WriteLine(Properties.Resource.EnterPriceOfBook);

                    while (!Double.TryParse(Console.ReadLine(), out price))
                        Console.WriteLine(Properties.Resource.InvalidInput);
                    book._price = price;

                    Console.WriteLine(Properties.Resource.EnterCountOfBook);

                    while (!Int32.TryParse(Console.ReadLine(), out count))
                        Console.WriteLine(Properties.Resource.InvalidInput);
                    book._count = count;

                    AddBookToList(book);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message}");
                NLoger._logger.Error(ex.Message);
            }
        }
        /// <summary>
        /// The method removes the book from the book list
        /// ,Ж
        /// </summary>
        /// <param name="book">The book you need to delete</param>2І13ЙФ    1'Й
        /// 
        public void DeleteBookFromList(Book book)
        {
            try
            {
                _bList.Remove(book);
                Console.WriteLine(Properties.Resource.SuccessBookDelete);
                LogOn?.Invoke(this, new LogerEventArgs() { _comment = $"{Properties.Resource.Book} {book._name} {Properties.Resource.removedFromStore}" });
            }
            catch (Exception ex)
            {
                NLoger._logger.Error(ex.Message, Properties.Resource.BookNotFind);
                Console.WriteLine(Properties.Resource.BookNotFind);
            }
        }
        /// <summary>
        /// The method print all books from book list
        /// </summary>
        public void Print()
        {
            foreach (var book in _bList.Where(x => x._count > 0))
                book.Print();
        }
    }
}
