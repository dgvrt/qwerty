﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore
{
    /// <summary>
    /// A class that describes the structure of the commentary to the book.
    /// </summary>
    public class Comment
    {
        /// <summary>
        /// The property сontains the nickname of the user who left the comment
        /// </summary>
        public string _nickname { get; set; }
        /// <summary>
        /// The fields contain the text of the comment
        /// </summary>
        public string _text;
        /// <summary>
        /// The property contains date when the comment was left 
        /// </summary>
        public DateTime _dateTime { get; set; }

        public Comment() { }
        /// <summary>
        /// Initializes a new comment object with parametres
        /// </summary>
        /// <param name="nickname">nickname of the user who left the comment</param>
        /// <param name="text">text of the comment</param>
        public Comment(string nickname, string text)
        {
            _nickname = nickname;
            _text = text;
            _dateTime = DateTime.Now;
        }
        /// <summary>
        /// Initializes a new comment object with parametres
        /// </summary>
        /// <param name="nickname">nickname of the user who left the comment</param>
        /// <param name="text">text of the comment</param>
        /// <param name="dt">date when the comment was left</param>
        public Comment(string nickname, string text, DateTime dt)
        {
            _nickname = nickname;
            _text = text;
            _dateTime = dt;
        }
    }
}
