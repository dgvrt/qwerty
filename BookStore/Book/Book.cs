﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using BookStore.Interfaces;

namespace BookStore
{
    /// <summary>
    /// A class that describes the structure of the book and contains a set of methods to work with it.
    /// </summary>
    [Serializable]
    public class Book : IComment, IPrint, IBook
    { 
        /// <summary>
        /// The property contains book name
        /// </summary>
        public string _name { get; set; }
        /// <summary>
        /// The property contains author name
        /// </summary>
        public string _author { get; set; }
        /// <summary>
        /// The property contains description for book
        /// </summary>
        public string _description { get; set; }
        /// <summary>
        /// The property contains book price
        /// </summary>
        public double _price { get; set; }
        /// <summary>
        /// The property contains information about numbers of book
        /// </summary>
        public int _count { get; set; }
        /// <summary>
        /// The property contains a list of comment for the current book
        /// </summary>
        public List<Comment> _commentList = new List<Comment>();
        public event EventHandler<LogerEventArgs> LogOn;

        /// <summary>
        /// Initializes a new book object without parametres
        /// </summary>      
        public Book(){}
        /// <summary>
        /// Initializes a new book object with parametres
        /// </summary>
        /// <param name="name">Book name</param>
        /// <param name="author">Book author</param>
        /// <param name="description">Book description</param>
        /// <param name="price">Book price</param>
        /// <param name="count">Numbers of book</param>
        public Book(string name, string author, string description, double price, int count)
        {
            _name = name;
            _author = author;
            _description = description;
            _price = price;
            _count = count;
        }

        /// <summary>
        /// Method use to change book count
        /// </summary>
        /// <param name="c">New count</param>
        public void ChangeBookCount(int c)
        {
            try
            {
                if (c >= 0)
                {
                    _count = c;
                    Console.WriteLine(Properties.Resource.NumberOfBooksSuccessfullyChanged);
                    LogOn?.Invoke(this, new LogerEventArgs() { _comment = $"{Properties.Resource.Add} {c} {Properties.Resource.Books} {_name}" });
                }
                else
                {
                    throw new Exception("Parametr less than 0!");
                }
            }
            catch (Exception ex)
            {
                NLoger._logger.Error(ex.Message);
            }
        }
        /// <summary>
        /// Method use to add comment about book
        /// </summary>
        /// <param name="comment">Comment to add</param>
        public void AddComment(Comment comment)
        {
            try
            {
                _commentList.Add(comment);
                LogOn?.Invoke(this, new LogerEventArgs() { _comment = $"{Properties.Resource.CommentSuccessfullyAddedForBook} {_name}" });
                Console.WriteLine($"{Properties.Resource.CommentSuccessfullyAddedForBook} { _name}");
            }
            catch(Exception ex)
            {
                NLoger._logger.Error(ex.Message);
                Console.WriteLine(Properties.Resource.CommentNotBeenAdded);
            }
        }
        /// <summary>
        /// Method use to delete some comment about book
        /// </summary>
        /// <param name="comment">Comment to delete</param>
        public void DeleteComment(Comment comment)
        {
            try
            {
                _commentList.Remove(comment);
                Console.WriteLine(Properties.Resource.CommentSuccessfullyDeleted);
                LogOn?.Invoke(this, new LogerEventArgs() { _comment = $"{Properties.Resource.CommentSuccessfullyRemovedForBook}{_name}" });
            }
            catch (Exception ex)
            {
                NLoger._logger.Error(ex.Message, Properties.Resource.CommentNotFind);
                LogOn?.Invoke(this, new LogerEventArgs() {_message = ex.Message ,_comment = Properties.Resource.CommentNotFind });
                Console.WriteLine(Properties.Resource.CommentNotFind);
            }
        }
        /// <summary>
        /// Method use to change book price
        /// </summary>
        /// <param name="p">New price</param>
        public void ChangePrice(double p)
        {
            try
            {
                if (p < 0)
                    throw new Exception(Properties.Resource.PriceLessThen0);
                _price = p;
                Console.WriteLine(Properties.Resource.SuccessfullyBookPriceChange);
                LogOn?.Invoke(this, new LogerEventArgs() { _comment = $"{Properties.Resource.PriceChangedForBook} {_name}" });
            }
            catch (Exception ex)
            {
                NLoger._logger.Error(ex.Message);
                LogOn?.Invoke(this, new LogerEventArgs() { _message = ex.Message});
                Console.WriteLine(Properties.Resource.PriceLessThen0);
            }
            
        }
        /// <summary>
        /// Method use to change book description
        /// </summary>
        /// <param name="s">New description</param>
        public void ChangeDescription(string s)
        {
            _description = s;
            Console.WriteLine(Properties.Resource.BookDescriptionSuccessfullyChange);
        }
        /// <summary>
        /// Method use to print all book comments to console
        /// </summary>
        public void PrintComments()
        {
            foreach (var c in _commentList)
            {
                Console.WriteLine($"{Properties.Resource.Nickname}: {c._nickname}, {Properties.Resource.Date}: {c._dateTime}.\n{c._text}.");
            }
        }
        /// <summary>
        /// Method use to print information about book to console
        /// </summary>
        public void Print()
        {
            Console.WriteLine($"{Properties.Resource.Name}: {_name};\n{Properties.Resource.Author}: {_author};" +
                $"\n{Properties.Resource.Description}: {_description};\n{Properties.Resource.Price}: {_price}.");
        }
    }
}
