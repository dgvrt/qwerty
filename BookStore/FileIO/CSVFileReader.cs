﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Microsoft.Office.Core;
using Microsoft.VisualBasic.FileIO;

namespace BookStore
{
    /// <summary>
    /// Class use to read information about object from the .csv file
    /// </summary>
    public static class CSVFileReader
    {
        /// <summary>
        /// Method read users list from file
        /// </summary>
        /// <param name="ulist">User list to store read information</param>
        public static void ReadFromFile(List<User> ulist)
        {
            string[] tmp;
            using (TextFieldParser tfp = new TextFieldParser($"{Directory.GetCurrentDirectory()}\\UserDB.csv", System.Text.Encoding.Default))
            {
                tfp.TextFieldType = FieldType.Delimited;
                tfp.SetDelimiters(",");
                while (!tfp.EndOfData)
                {
                    tmp = tfp.ReadFields();
                    Int32.TryParse(tmp[0], out int id);
                    Double.TryParse(tmp[4], out double wallet);
                    Int32.TryParse(tmp[5], out int access);
                    User user = new User(){_id = id, _login  = tmp[1], _password = tmp[2], _nickname = tmp[3],
                        _wallet = wallet, _access =  access};
                    ulist.Add(user);
                }
            }
        }
        /// <summary>
        /// Method read book list from the file
        /// </summary>
        /// <param name="blist">Book list to store read information</param>
        public static void ReadFromFile(BookList blist)
        {
            string[] tmp;
            string[] tmp1;
            using (TextFieldParser tfp = new TextFieldParser($"{Directory.GetCurrentDirectory()}\\BookDB.csv", System.Text.Encoding.Default))
            {
                tfp.TextFieldType = FieldType.Delimited;
                tfp.SetDelimiters("|");
                while (!tfp.EndOfData)
                {
                    tmp = tfp.ReadFields();
                    tmp1 = tmp[0].Split(',');
                    Double.TryParse(tmp1[4], out double price);
                    Int32.TryParse(tmp1[3], out int count);
                    Book book = new Book(tmp1[0], tmp1[1], tmp1[2], price, count);
                    int i = 1;
                    while (i < tmp.Length)
                    {
                        tmp1 = tmp[i].Split(',');
                        book.AddComment(new Comment(tmp1[0], tmp1[1], Convert.ToDateTime(tmp1[2])){});
                        i++;
                    }
                    blist.AddBookToList(book);
                }
            }
        }
    }
}
