﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BookStore
{
    /// <summary>
    /// Class for write information in the .csv file
    /// </summary>
    public static class CSVFileWriter
    {
        /// <summary>
        /// Method write user list in the file
        /// </summary>
        /// <param name="ulist">User list to save</param>
        public static void WriteToFile(List<User> ulist)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter($"{Directory.GetCurrentDirectory()}\\UserDB.csv",
                    false, System.Text.Encoding.Default))
                {
                    foreach (var user in ulist)
                        sw.WriteLine($"{user._id},{user._login},{user._password},{user._nickname},{user._wallet},{user._access}");
                    //$"{String.Join("|", user._wishlist._wishList.Select(x => x._name + ","))}" + 
                    //$"{String.Join("|", user._boughtBooks._bList.Select(x => x._name) + ",")}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        /// <summary>
        /// Method write book list in the file
        /// </summary>
        /// <param name="bookList">Book list to save</param>
        public static void WriteToFile(BookList bookList)
        {
            string tmp = "";
            try
            {
                using (StreamWriter sw = new StreamWriter($"{Directory.GetCurrentDirectory()}\\BookDB.csv",
                    false, System.Text.Encoding.Default))
                {
                    foreach (var book in bookList._bList)
                    {
                        tmp = $"{book._name},{book._author},{book._description},{book._count},{book._price}";
                        foreach (Comment c in book._commentList)
                        tmp += $"|{c._nickname},{c._text},{c._dateTime}";
                        sw.WriteLine(tmp);
                    }
                                         
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
