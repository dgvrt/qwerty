﻿using BookStore.Interfaces;
using BookStore.DataIO;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BookStore
{
    /// <summary>
    /// The class launches a bookstore, implements the menu and the entire life cycle of the store
    /// </summary>
    public class Launching : IRunnable
    {
        /// <summary>
        /// Fields use to set current program culture 
        /// </summary>
        public static volatile string currentCulture;
        public static Dictionary<string, string> CultureDictionary =
            new Dictionary<string, string> { {"en", "en-US" }, { "ua", "uk-UA" } };
        /// <summary>
        /// List of all books in the store
        /// </summary>
        private BookList _blist;
        /// <summary>
        /// List of all users
        /// </summary>
        private List<User> _ulist;
        /// <summary>
        /// The current user with whom the program is running
        /// </summary>
        User _currentUser;
        CustomLogger customLogger;

        public Launching()
        {
            _currentUser = new User();
            customLogger = new CustomLogger();                
            //CSVFileReader.ReadFromFile(_blist);
            //CSVFileReader.ReadFromFile(_ulist);
        }

        /// <summary>
        /// Method run BookStore
        /// </summary>
        public new void Run()
        {                     
            bool _continue = false;
            string _action;
            SetCurrentCulture();
            _blist = DataReader.BooksDataReadAsync().Result;
            _ulist = DataReader.UsersDataReadAsync().Result;
            Console.Clear();
            Console.WriteLine($"{Properties.Resource.PleaseSelectOneOfTheFollowingAction} :\n(1){Properties.Resource.Login}\n(2){Properties.Resource.Registration}");
            _action = Console.ReadLine();
            while (!_continue)
                switch (_action)
                {
                    case "1":
                        _currentUser = LogIn.Login(_ulist);
                        _continue = true;
                        break;
                    case "2":
                        _currentUser = Registration.Register(_ulist);
                        _continue = true;
                        break;
                    default:
                        Console.WriteLine(Properties.Resource.InvalidInput);
                        _action = Console.ReadLine();
                        break;
                }
            Console.WriteLine($"{Properties.Resource.WelcomeToOurBookStore} {_currentUser._nickname}!");
            _currentUser.LogOn += customLogger.Log;
            _blist.LogOn += customLogger.Log;
            PrintMainMenu();
            while (_continue)
            {
                _action = Console.ReadLine();
                switch (_action)
                {
                    case "1":
                        _blist.Print();
                        break;
                    case "2":
                        UserMenu();
                        break;
                    case "3":
                        AdminPanel();
                        break;
                    case "4":
                        Exit();
                        return;
                    default:
                        if (_blist.SearchBook(_action) != null)
                        { BookMenu(_blist.SearchBook(_action)); break; }
                        else
                        {
                            Console.WriteLine(Properties.Resource.InvalidBookInput);
                            _action = Console.ReadLine();
                            break;
                        }
                }
            }

        }

        private void BookMenu(Book book)
        {
            string tmp;
            bool _continue = false;
            string action;
            Console.WriteLine("--------------");
            book.Print();
            PrintBookMenu();
            while (!_continue)
            {
                action = Console.ReadLine();
                switch (action)
                {
                    case "1":
                        if (_currentUser.BuyABook(book))
                            Console.WriteLine(Properties.Resource.TheBookHasBeenSuccessfullyPurchased);
                        break;
                    case "2":
                        Console.WriteLine(Properties.Resource.EnterComment);
                        tmp = Console.ReadLine();
                        book.AddComment(new Comment(_currentUser._nickname, tmp));
                        Console.WriteLine(Properties.Resource.CommentSuccessfully);
                        break;
                    case "3":
                        _currentUser._wishlist.AddBookToWishList(book);
                        Console.WriteLine(Properties.Resource.BookSuccessfullyAdded);
                        break;
                    case "4":
                        book.PrintComments();
                        break;
                    case "5":
                        Console.Clear();
                        PrintMainMenu();
                        _continue = true;
                        break;
                }
            }
        }
        private void UserMenu()
        {
            bool _continue = false;
            string action;
            _currentUser.Print();
            PrintUserMenu();
            while (!_continue)
            {
                action = Console.ReadLine();
                switch (action)
                {
                    case "1":
                        _currentUser._wishlist.PrintWishList();
                        break;
                    case "2":
                        _currentUser._boughtBooks.Print();
                        break;
                    case "3":
                        Console.Clear();
                        PrintMainMenu();
                        _continue = true;
                        break;
                    default:
                        Console.WriteLine(Properties.Resource.InvalidInput);
                        action = Console.ReadLine();
                        break;
                }
            }
        }
        private void AdminPanel()
        {
            bool _continue = false;
            string action;
            if (_currentUser._access == 0)
                Console.WriteLine(Properties.Resource.NoAccess);
            else
            {
                PrintAdminMenu();
                while (!_continue)
                {
                    action = Console.ReadLine();
                    switch (action)
                    {
                        case "1":
                            _blist.AddBookToList();
                            break;
                        case "2":
                            _blist.Print();
                            break;
                        case "3":
                            Console.Clear();
                            PrintMainMenu();
                            _continue = true;
                            break;
                        default:
                            if (_blist.SearchBook(action) != null)
                            { AdminBookMenu(_blist.SearchBook(action)); break; }
                            else
                            {
                                Console.WriteLine(Properties.Resource.InvalidBookInput);
                                action = Console.ReadLine();
                                break;
                            }

                    }
                }
            }
        }
        private void AdminBookMenu(Book book)
        {
            bool _continue = false;
            string action;
            PrintAdminBookMenu();
            while (!_continue)
            {
                action = Console.ReadLine();
                switch (action)
                {
                    case "1":
                        Console.WriteLine(Properties.Resource.EnterBookNumber);
                        int c;
                        while(!Int32.TryParse(Console.ReadLine(), out c))
                        Console.WriteLine(Properties.Resource.InvalidBookInput);
                        book.ChangeBookCount(c);
                        break;
                    case "2":
                        Console.WriteLine(Properties.Resource.EnterBookPrice);
                        double p;
                        while (!Double.TryParse(Console.ReadLine(), out p))
                        Console.WriteLine(Properties.Resource.InvalidBookInput);
                        book.ChangePrice(p);
                        break;
                    case "3":
                        Console.WriteLine(Properties.Resource.EnterBookDescription);
                        book.ChangeDescription(Console.ReadLine());
                        break;
                    case "4":
                        _blist.DeleteBookFromList(book);
                        break;
                    case "5":
                        Console.Clear();
                        PrintAdminMenu();
                        _continue = true;
                        break;
                }
            }
        }
        private void Exit()
        {
            DataWriter.DataWriteAsync(_blist, _ulist);
            //CSVFileWriter.WriteToFile(_blist);
            //CSVFileWriter.WriteToFile(_ulist);
        }
        private void SetCurrentCulture()
        {
            bool _continue = false;
            string _action;
            while (!_continue)
            {
                Console.WriteLine("Please select a language:\n(1)English\n(2)Ukrainian");
                _action = Console.ReadLine();
                switch (_action)
                {
                    case "1":
                        currentCulture = "en";
                        _continue = true;
                        break;
                    case "2":
                        currentCulture = "ua";
                        _continue = true;
                        break;
                    default:
                        Console.WriteLine("Wrong input!");
                        break;
                }
            }
            string culture = CultureDictionary[currentCulture];
            CultureInfo ci = new CultureInfo(culture);
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;
        }

        private void PrintMainMenu()
        {
            Console.WriteLine($"{Properties.Resource.MainMenu}\n(1){Properties.Resource.AvailableBooks}\n(2){Properties.Resource.AccountInformation}\n(3){Properties.Resource.AdminPanel}\n" +
                $"(4){Properties.Resource.Exit}");
        }
        private void PrintBookMenu()
        {
            Console.WriteLine($"(1){Properties.Resource.BuyBook}\n(2){Properties.Resource.LeaveComment}\n(3){Properties.Resource.AddToWishList}\n" +
                $"(4){Properties.Resource.ViewComments}\n(5){Properties.Resource.ReturnToMainMenu}");
        }
        private void PrintUserMenu()
        {
            Console.WriteLine($"(1){Properties.Resource.ViewWishList}\n(2){Properties.Resource.ViewPurchasedBooks}\n(3){Properties.Resource.ReturnToMainMenu}");
        }
        private void PrintAdminMenu()
        {
            Console.WriteLine($"(1){Properties.Resource.AddBook}\n(2){Properties.Resource.ViewListOfBooks}\n(3){Properties.Resource.ReturnToMainMenu}\n{Properties.Resource.EnterBookName}");
        }
        private void PrintAdminBookMenu()
        {
            Console.WriteLine($"(1){Properties.Resource.ChangeTheNumberOfBooks}\n(2){Properties.Resource.ChangeThePrice}\n" +
                $"(3){Properties.Resource.ChangeDescription}\n(4){Properties.Resource.DeleteBook}\n(5){Properties.Resource.Return}");
        }

    }
}
