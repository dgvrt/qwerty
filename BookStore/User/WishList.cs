﻿using BookStore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore
{
    /// <summary>
    ///The class implements a list of desirable user books
    /// </summary>
    [Serializable]
    public class WishList : IWishList
    {
        public event EventHandler<LogerEventArgs> LogOn;
        public List<Book> _wishList;

        public WishList()
        {
            _wishList = new List<Book>();
        }
        /// <summary>
        /// Method add  desirable book to the list
        /// </summary>
        /// <param name="b"> desirable book</param>
        public void AddBookToWishList(Book b)
        {
            try
            {
                _wishList.Add(b);
                LogOn?.Invoke(this, new LogerEventArgs() { _comment = $"{Properties.Resource.Book} {b._name} {Properties.Resource.BookAddToWish}" });
            }
            catch (Exception ex)
            {
                NLoger._logger.Error(ex.Message);
            }
        }
        /// <summary>
        /// Method delete book from the wish list
        /// </summary>
        /// <param name="b">Book to delete</param>
        public void DeleteBookFromWishList(Book b)
        {
            try
            {
                _wishList.Remove(b);
            }
            catch (Exception ex)
            {
                NLoger._logger.Error(ex.Message, Properties.Resource.BookNotFindInWishList);
                Console.WriteLine(Properties.Resource.BookNotFindInWishList);
            }
        }
        /// <summary>
        /// Method return user wish list
        /// </summary>
        /// <returns>user wish list</returns>
        public List<Book> GetWishList()
        {
            return _wishList;
        }
        /// <summary>
        /// Print user wish list
        /// </summary>
        public void PrintWishList()
        {
            Console.WriteLine($"{Properties.Resource.WishListContain} {_wishList.Count} {Properties.Resource._books}: ");
            foreach (var b in _wishList)
                b.Print();
        }
    }
}
