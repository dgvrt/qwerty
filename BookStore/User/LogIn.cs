﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace BookStore
{
    /// <summary>
    /// The class implements logging
    /// </summary>
    public static class LogIn
    {
        /// <summary>
        /// The method performs logging of the user in a bookstore
        /// </summary>
        /// <param name="ulist">List of all users</param>
        /// <returns>Returns the user who is logged in</returns>
        public static User Login(List<User> ulist)
        {
            bool check = false;
            string tmp;
            User user = new User();
            while (!check)
            {
                Console.WriteLine(Properties.Resource.EnterLogin);
                tmp = Console.ReadLine();
                if (ulist.Find(x => x._login == tmp) != null)
                {
                    user = ulist.Find(x => x._login == tmp);
                    check = true;
                }
                else
                {
                    Console.WriteLine(Properties.Resource.UserNotFound);
                }
            }
            while (check)
            {
                Console.WriteLine(Properties.Resource.EnterPassword);
                tmp = Console.ReadLine();
                if (user._password == tmp)
                {
                    Console.Clear();                   
                    return user;
                }
                else
                {
                    Console.WriteLine(Properties.Resource.InvalidPassword);
                }
            }
            return user;
        }
    }
}
