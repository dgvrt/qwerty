﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookStore.Interfaces;

namespace BookStore
{
    /// <summary>
    /// The class implements a book store user
    /// </summary>
    [Serializable]
    public class User : IPrint
    {
        /// <summary>
        /// Unique id
        /// </summary>
        public int _id { get; set; }
        /// <summary>
        /// User login
        /// </summary>
        public string _login { get; set; }
        /// <summary>
        /// User password
        /// </summary>
        public string _password { get; set; }
        /// <summary>
        /// User password
        /// </summary>
        public string _nickname { get; set; }
        /// <summary>
        /// User wallet
        /// </summary>
        public double _wallet { get; set; }
        /// <summary>
        /// User access permission;
        /// </summary>
        public double _access { get; set; }
        /// <summary>
        /// User wish list
        /// </summary>
        public WishList _wishlist;
        /// <summary>
        /// List of books bought by the user
        /// </summary>
        public BookList _boughtBooks;
        public event EventHandler<LogerEventArgs> LogOn;

        public User()
        {
            _access = 0;
            _wishlist = new WishList();
            _boughtBooks = new BookList();
            _wallet = new Random().Next(100, 300);
        }
        /// <summary>
        /// Method for set user password
        /// </summary>
        /// <param name="s">User password</param>
        public void SetPassword(string s)
        {
            try
            {
                if (s.Length < 8)
                {
                    throw new Exception(Properties.Resource.PasswordLenghtLess);
                }
                else _password = s;
            }
            catch (Exception ex)
            {
                NLoger._logger.Error(ex.Message);
                Console.WriteLine(ex.Message + $"\n{Properties.Resource.EnterNewPassword}");
                SetPassword(Console.ReadLine());
            }
        }
        /// <summary>
        /// The method implements the purchase of a book
        /// </summary>
        /// <param name="book">The book is what the user wants to buy</param>
        /// <returns>True, if the purchase is successful, false if not</returns>
        public bool BuyABook(Book book)
        {
            try
            {
                if ((_wallet - book._price) > 0)
                {
                    _boughtBooks.AddBookToList(book);
                    _wallet -= book._price;
                    LogOn?.Invoke(this, new LogerEventArgs() { _comment = $"{_nickname} {Properties.Resource.bookBuy} {book._name}" });
                    return true;
                }
                else
                {
                    throw new Exception(Properties.Resource.NotEnoughMoney);
                }
            }
            catch (Exception ex)
            {
                NLoger._logger.Error(ex.Message);
                return false;
            }
        }
        /// <summary>
        /// Method print information about user
        /// </summary>
        public void Print()
        {
            Console.WriteLine($"Id: {_id};\n{Properties.Resource.Nickname}: {_nickname};\n{Properties.Resource.ULogin}: {_login};\n" +
                $"{Properties.Resource.Wallet}: {_wallet}$\n{Properties.Resource.BoughtBook}: {_boughtBooks._bList.Count()}" +
                $"\n{Properties.Resource.BooksInWishList}: {_wishlist.GetWishList().Count()}");
        }
    }
}
