﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore
{
    /// <summary>
    /// The class implements user registration
    /// </summary>
    public static class Registration
    {
        /// <summary>
        /// The method registers the user in the system
        /// </summary>
        /// <param name="ulist">List of all users</param>
        /// <returns>User who is registered on the system</returns>
        public static User Register(List<User> ulist)
        {
            User user = new User();
            bool check = false;
            string tmp;
            Console.WriteLine(Properties.Resource.RegistrationMenu);
            while (!check)
            {
                Console.WriteLine($"{Properties.Resource.ULogin}: ");
                tmp = Console.ReadLine();
                if (CheckLogin(tmp, ulist))
                {
                    user._login = tmp;
                    check = true;
                }
                else
                    Console.WriteLine(Properties.Resource.LoginUse);
            }
                Console.WriteLine(Properties.Resource.PasswordCharacters);
                tmp = Console.ReadLine();
                user.SetPassword(tmp);
            while (check)
            {
                Console.WriteLine($"{Properties.Resource.Nickname}: ");
                tmp = Console.ReadLine();
                if (CheckLogin(tmp, ulist))
                {
                    user._nickname = tmp;
                    user._id = ulist.Count() + 1;
                    check = false;
                }
                else
                    Console.WriteLine(Properties.Resource.NicknameUse);
            }
            Console.WriteLine($"{Properties.Resource.Congratulations} {user._nickname}, {Properties.Resource.SuccessfullySignedUp}");           
            ulist.Add(user);
            return user;

        }
        /// <summary>
        /// The method checks whether this login is free
        /// </summary>
        /// <param name="tmp">User login</param>
        /// <param name="ulist">List of all users</param>
        /// <returns>True if login is free and false if not </returns>
        private static bool CheckLogin(string tmp, List<User> ulist)
        {
            if (ulist.Find(x => x._login == tmp) != null)
                return false;
            else return true;
        }
        /// <summary>
        /// The method checks whether this nickname is free
        /// </summary>
        /// <param name="tmp">User nickname</param>
        /// <param name="ulist">List of all users</param>
        /// <returns>True if nickname is free and false if not </returns>
        private static bool CheckNickname(string tmp, List<User> ulist)
        {
            if (ulist.Find(x => x._nickname == tmp) != null)
                return false;
            else return true;
        }
    }
}
