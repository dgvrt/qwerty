﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BookStore
{
    public class CustomLogger
    {

        public void Log(object sender, LogerEventArgs e)
        {
            //string path = $"{Directory.GetCurrentDirectory()}\\{DateTime.Now.ToShortDateString()}.log";
            try
            {
                using (StreamWriter sw = new StreamWriter($"{Directory.GetCurrentDirectory()}\\{DateTime.Now.ToString("dd.MM.yyyy")}.log",
                    true, System.Text.Encoding.Default))
                {
                    sw.WriteLine($"{DateTime.Now} {e._comment}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
