﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore
{
    public class LogerEventArgs : EventArgs
    {
        public string _message { get; set; }
        public string _comment { get; set; }
    }
}
